from .models import Matrix
from django.forms import ModelForm, Textarea


class MatrixForm(ModelForm):
    class Meta:
        model = Matrix
        fields = ['matrix']
        widgets = {
            'matrix': Textarea(attrs={
                'id': 'matrix',
                'placeholder': '2 -2 1 = -3&#13;&#10;1 3 -2 = 1&#13;&#10;3 -1 -1 = 2',
                'class': 'form-control',
                'style': "resize: both",
                'cols': '30',
                'rows': '4'
            })
        }
