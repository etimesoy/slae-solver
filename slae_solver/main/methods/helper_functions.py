from typing import List


def convert(matrix: str) -> List[List[int]]:
    """
    Converts matrix from string type to List[List[int]] type

    :param matrix: matrix from user's input
    :return: matrix with integer items
    """
    int_matrix = []
    for line in matrix.split('\r\n'):
        int_matrix.append(line.split())
    if int_matrix[0][-2] == '=':
        for j in int_matrix:
            del j[-2]
    for i in range(len(int_matrix)):
        for j in range(len(int_matrix[0])):
            int_matrix[i][j] = int(int_matrix[i][j])
    return int_matrix


def get_determinant(matrix: List[List[int]]) -> int:
    """
    Finds the determinant of a matrix using recursion

    :param matrix: matrix with integer numbers
    :return: determinant of matrix
    """
    determinant = 0
    if len(matrix) == 1:
        return matrix[0][0]
    elif len(matrix[0]) == 2 and len(matrix) == 2:
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    elif len(matrix[0]) == len(matrix):
        for i in range(len(matrix)):
            new_matrix = []
            new_line = []
            coefficient = pow(-1, i) * matrix[i][0]
            for j in range(len(matrix)):
                for h in range(len(matrix)):
                    if j != i and h != 0:
                        new_line.append(matrix[j][h])
                if len(new_line) > 0:
                    new_matrix.append(new_line)
                    new_line = []
            determinant += coefficient * get_determinant(new_matrix)
        return determinant
    else:
        return 0
