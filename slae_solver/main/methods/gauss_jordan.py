from typing import List


def gauss_jordan(matrix: List[List[int]]) -> str:
    """
    Solves SLAE using Gauss-Jordan method

    :param matrix: extended SLAE
    :return: string with answers, separated by spaces
    """
    processed = []
    quantity = len(matrix)
    for i in range(quantity):
        minn = 10000000000000000000000000000000000000
        n_minn = 0
        check = True
        for j in range(0, quantity):
            if j not in processed and abs(matrix[j][i]) < abs(minn) and matrix[j][i] != 0:
                minn = matrix[j][i]
                n_minn = j
                check = False
            elif len(processed) == quantity:
                for k in range(quantity):
                    if k not in processed:
                        minn = matrix[k][i]
                        n_minn = k
                        check = False
        if check:
            processed.append(0)
        else:
            processed.append(n_minn)
        list_min = [minn] * (quantity + 1)
        matrix[n_minn], list_min = division_line(matrix[n_minn], list_min)
        for h in range(quantity):
            if h != n_minn:
                coefficient = matrix[h][i]
                matrix[h] = [x - y * coefficient for x, y in zip(matrix[h], matrix[n_minn])]
    return get_answer(matrix, quantity)


def division_line(line1, line2):
    for i in range(len(line1)):
        line1[i] /= line2[i]
    return line1, line2


def get_answer(matrix, quantity):
    answer = [0] * quantity
    for i in range(quantity):
        for j in range(quantity):
            if matrix[i][j] == 1:
                answer[j] = round(matrix[i][quantity], 1)
    str_matrix = ''
    for x in answer:
        str_matrix += str(x) + ' '
    return str_matrix
