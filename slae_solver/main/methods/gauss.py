import copy
from typing import List


def gauss(matrix: List[List[int]]) -> str:
    """
    Solves SLAE using Gauss method

    :param matrix: extended SLAE
    :return: string with answers, separated by spaces
    """
    # matrix - расширенная матрица
    matrix_len = len(matrix)  # Количество строк в начальной матрице
    matrix_wid = len(matrix[0])  # Размерность строки начальной матрицы
    matrix_clone = copy.deepcopy(matrix)  # Матрица-дублер

    # Прямой ход (Зануление нижнего левого угла)
    for k in range(matrix_len):  # k-номер строки
        if matrix[k][k] == 0:
            continue
        for i in range(matrix_wid):  # i-номер столбца
            # Деление k-строки на первый член !=0 для преобразования его в единицу
            matrix_clone[k][i] /= matrix[k][k]
        for i in range(k + 1, matrix_len):  # i-номер следующей строки после k
            q = matrix_clone[i][k] / matrix_clone[k][k]  # Коэффициент
            for j in range(matrix_wid):  # j-номер столбца следующей строки после k
                # Зануление элементов матрицы ниже первого члена, преобразованного в единицу
                matrix_clone[i][j] -= matrix_clone[k][j] * q
        for i in range(matrix_len):  # Обновление, внесение изменений в начальную матрицу
            for j in range(matrix_wid):
                matrix[i][j] = matrix_clone[i][j]

    # Обратный ход (Зануление верхнего правого угла)
    k_idx = -2
    for k in range(matrix_len - 1, -1, -1):
        if matrix[k][k_idx] == 0:
            continue
        matrix_clone_k_k_idx = matrix_clone[k][k_idx]
        for i in range(matrix_wid - 1, -1, -1):
            matrix_clone[k][i] /= matrix_clone_k_k_idx
        for i in range(k - 1, -1, -1):
            q = matrix_clone[i][k_idx]
            for j in range(matrix_wid - 1, -1, -1):
                matrix_clone[i][j] -= matrix_clone[k][j] * q
        k_idx -= 1

    # Отделяем от общей матрицы ответы
    answer = ''
    for i in range(matrix_len):
        answer += str(matrix_clone[i][-1]) + ' '

    return answer
