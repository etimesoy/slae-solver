import copy
from typing import List
from .helper_functions import get_determinant


def kramer(matrix: List[List[int]]) -> str:
    """
    Solves SLAE using Kramer method

    :param matrix: extended SLAE
    :return: string with answers
    """
    free_term = []
    for i in range(len(matrix)):
        #  фильтрует коэффициенты х-ов и свободные члены в отдельные списки
        free_term.append(matrix[i][-1])
        matrix[i].pop(-1)

    main_determinant = get_determinant(matrix)
    quantity = len(matrix)
    list_x = ''
    if main_determinant != 0:
        for i in range(quantity):
            new_matrix = copy.deepcopy(matrix)  # создаем копию, чтобы не изменить основну СЛАУ
            for j in range(quantity):
                for h in range(quantity):
                    new_matrix[j][i] = free_term[j]  # Заменяем стобцы на свободные члены
            list_x += str(int(get_determinant(new_matrix) / main_determinant)) + ' '
        return list_x
    else:
        return "Данная система не может быть решена методом Крамера"
