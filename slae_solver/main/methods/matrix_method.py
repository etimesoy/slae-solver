import copy
from typing import List
from .helper_functions import get_determinant


def transpose(matrix: List[List[int]]) -> List[List[int]]:
    """
    Transposes matrix

    :param matrix: matrix with integer numbers
    :return: transposed matrix
    """
    result = []
    row = len(matrix)
    column = len(matrix[0])
    for j in range(column):
        tmp = []
        for i in range(row):
            tmp = tmp + [matrix[i][j]]
        result = result + [tmp]
    return result


def matrix_method(matrix: List[List[int]]) -> str:
    """
    Solves SLAE using matrix method

    :param matrix: extended SLAE
    :return: string with answers
    """
    free_term = []
    for i in range(len(matrix)):
        #  фильтрует коэффициенты х-ов и свободные члены в отдельные списки
        free_term.append(matrix[i][-1])
        matrix[i].pop(-1)
    main_determinant = get_determinant(matrix)
    quantity = len(matrix)
    if main_determinant != 0:
        inverse_matrix = copy.deepcopy(matrix)  # создаем копию, чтобы не изменить основную СЛАУ
        minor = []
        for i in range(quantity):  # находим обратную матрицу
            for j in range(quantity):
                for h in range(quantity):
                    if h != i:
                        minor.append([])
                        for y in range(quantity):
                            if y != j:
                                minor[-1].append(matrix[h][y])
                inverse_matrix[i][j] = pow(-1, i + j) * get_determinant(minor)
                minor = []
        new_matrix = []
        inverse_matrix = transpose(inverse_matrix)  # транспонируем матрицу
        for i in range(len(inverse_matrix)):
            new_matrix.append(0)
        for i in range(len(inverse_matrix)):
            for k in range(len(inverse_matrix[0])):
                new_matrix[i] += inverse_matrix[i][k] * free_term[k]
        for i in range(len(new_matrix)):
            new_matrix[i] *= pow(main_determinant, -1)
        str_matrix = ''
        for x in new_matrix:
            str_matrix += str(x) + ' '
        return str_matrix
    else:
        return "Обратной матрицы НЕ СУЩЕСТВУЕТ"
