from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about_us, name='about'),
    path('my_matrices', views.my_matrices, name='my_matrices'),
    path('delete/<int:matrix_id>/', views.delete, name='delete')
]
