from typing import List
from random import randint


def random(n: int) -> List[List[int]]:
    """
    Creates a SLAE with random integer numbers

    :param n: count of lines in this SLAE
    :return: SLAE
    """
    matrix = []
    for i in range(n):
        line = []
        for j in range(n + 1):
            line.append(randint(-10, 10))
        matrix.append(line)
    return matrix
