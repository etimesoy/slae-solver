from typing import List
from methods.gauss import gauss
from methods.kramer import kramer
from methods.matrix_method import matrix_method
from methods.gauss_jordan import gauss_jordan


class Matrix:
    def __init__(self, matrix: List[List[int]]):
        self.matrix = matrix

    @property
    def gauss(self):
        return gauss(self.matrix)

    @property
    def kramer(self):
        return kramer(self.matrix)

    @property
    def matrix_method(self):
        return matrix_method(self.matrix)

    @property
    def gauss_jordan(self):
        return gauss_jordan(self.matrix)

    def solve(self, choose):
        if choose == 1:
            return self.gauss
        elif choose == 2:
            return self.kramer
        elif choose == 3:
            return self.matrix_method
        else:
            return self.gauss_jordan
