from classes import Matrix


def main():
    """
    This is a console version of project. It performs basic interaction with matrices and solving methods.

    :return:
    """
    number_of_lines = int(input('Введите количество строк в вашей матрицей: '))
    print('Введите расширенную матрицу значений, отделяя соседние цифры пробелом, а строки - клавишей enter')
    matrix = []
    for _ in range(number_of_lines):
        cur_line = [int(i) for i in input().split()]
        matrix.append(cur_line)
    matrix_1 = Matrix(matrix)
    choose = int(
        input('Выберите метод решения этой системы (1. Гаусса, 2. Крамера, 3. Матричный метод, 4. Гаусса-Джордана): '))
    print('Ответ =', matrix_1.solve(choose))


if __name__ == '__main__':
    main()
