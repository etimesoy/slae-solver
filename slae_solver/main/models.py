from django.db import models


class Matrix(models.Model):
    matrix = models.TextField('Матрица')
    answer = models.CharField(default='Решение', max_length=240)
    solution_method = models.CharField(default='Метод решения', max_length=120)

    def __str__(self):
        output_matrix = []
        for line in self.matrix:
            output_matrix.append(' '.join(str(x) for x in line))
        return '\n'.join(str(x) for x in output_matrix)

    def matrix_lines(self):
        lines_list = []
        cur_line = ''
        for x in self.matrix:
            if x != '\n':
                cur_line += x
            else:
                lines_list.append(cur_line)
                cur_line = ''
        lines_list.append(cur_line)
        return lines_list

    class Meta:
        verbose_name = 'Матрица'
        verbose_name_plural = 'Матрицы'
