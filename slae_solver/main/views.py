from django.shortcuts import render, redirect
from .models import Matrix
from .methods import gauss, kramer, matrix_method, gauss_jordan, helper_functions
from .matrix_randomization import randomization


def my_matrices(request):
    matrix_list = Matrix.objects.order_by('-id')
    return render(request, 'main/my_matrices.html', {'matrix_list': matrix_list})


def about_us(request):
    return render(request, 'main/about_us.html')


def index(request):
    context = {}

    if request.method == 'POST':
        methods_dict = {
            'Gauss': 'Метод Гаусса',
            'Kramer': 'Метод Крамера',
            'Matrix_method': 'Матричный метод',
            'Gauss_Jordan': 'Метод Гаусса-Джордана'
        }
        func_dict = {
            'Gauss': gauss.gauss,
            'Kramer': kramer.kramer,
            'Matrix_method': matrix_method.matrix_method,
            'Gauss_Jordan': gauss_jordan.gauss_jordan
        }
        if 'fill_with_random' in request.POST:
            int_matrix = randomization.random(int(request.POST['lines_count']))
            matrix = ''
            for i in range(len(int_matrix)):
                for j in range(len(int_matrix[0])):
                    matrix += str(int_matrix[i][j]) + ' '
                if i != len(int_matrix) - 1:
                    matrix += '\r\n'
            answer = func_dict[request.POST['solution_method']](int_matrix)
            solution_method = methods_dict[request.POST['solution_method']]
        else:
            matrix = request.POST.get('matrix')
            int_matrix = helper_functions.convert(matrix)
            for key, val in methods_dict.items():
                if key in request.POST:
                    answer = func_dict[key](int_matrix)
                    solution_method = val
        Matrix(answer=answer, matrix=matrix, solution_method=solution_method).save()
        context.update({
            'answer': answer,
            'matrix': [line for line in matrix.split('\r\n')],
            'solution_method': solution_method
        })

    return render(request, 'main/index.html', context)


def delete(request, matrix_id):
    matrix = Matrix.objects.get(id=matrix_id)
    matrix.delete()
    return redirect('my_matrices')
